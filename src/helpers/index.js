/**
 * Calculates the amount of letter in common between words
 * @param {*} guessedWord - word input by the user
 * @param {*} secretWord - word to match
 * @returns integer
 */
export function getLetterMatchCount(guessedWord, secretWord) {
  const secretLetterSet = new Set(secretWord.split(''));
  const guessedLetterSet = new Set(guessedWord.split(''));
  return [...secretLetterSet].filter(letter => guessedLetterSet.has(letter))
    .length;
}
